## DEVELOPER TEST
Berikut ini akan menjelaskan beberapa step penggunaan logic testing Developer
Script ini menggunakan Bahasa Pemrograman PHP dengan bantuan data .json untuk pengambilan datanya.
Ada 3 data yang diberikan yaitu productData, stockData dan locationData.
Alasan:
Alasan dalam pengerjaan testing ini dengan menggunakan bahasa Pemrograman PHP dikarenakan dapat menghandle data array pada controllernya dan menggabungkannya lalu data tersebut dapat digunakan kembali yang telah bergabung.

Analisa:
Dalam menganalisa project ini dapat susun lebih rapi dengan menggunakan Array dan terakhir di parsing ke bentuk json (sesuai kebutuhan).

keuntungan:
Keuntungannya yaitu data dapat dibaca lebih rapi dengan penggabungan data array di satu tempat saja, alangkah bagusnya data-data ini disimpan kedalam database agar dapat di kelola seperti penggunaan pagination (limit, offset) untuk handle banyak data.

kerugian:
dengan tidak adanya penggunaan database bisa berakibat fatal pada load banyak data.

# Petunjuk
- Install terlebih dahulu XAMPP atau menjalankan apache agar dapat mendeteksi PHP
- Jalankan aplikasi dengan mengakses http://localhost/orami

# Note
- untuk file index.php, didalam file ini hanya menggunakan file ini saja untuk menjalankan aplikasinya.
- sedangkan untuk file index2.php, dimana file membutuhkan file data .json yaitu (productData.json, stockData.json dan locationData.json) untuk men load data yang dimanfaatkan pada file index2.php ini.

Terimakasih
