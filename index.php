<?php 

$productData = '[
    {
      "productId": 1000,
      "productName": "Product 1000"
    },
    {
      "productId": 1001,
      "productName": "Product 1001"
    }
  ]'; 
$productData_r = json_decode($productData, true); 

$stockData = '[
  {
    "productId": 1000,
    "locationId": 1,
    "stock": 21
  },
  {
    "productId": 1000,
    "locationId": 2,
    "stock": 8
  },
  {
    "productId": 1001,
    "locationId": 1,
    "stock": 4
  },
  {
    "productId": 1001,
    "locationId": 2,
    "stock": 10
  }
]';
$stockData_r = json_decode($stockData, true);

//Bantuan Pemanggilan Nama Lokasi
function locationHelp($locationId){
    $locationData = '[
        {
            "locationId": 1,
            "locationName": "Location 1"
        },
        {
            "locationId": 2,
            "locationName": "Location 2"
        }
    ]';
    $locationData_r = json_decode($locationData, true);

    foreach ($locationData_r as $key => $val) {
        if($locationId == $val['locationId']){
            return $val['locationName'];
        }
    }
}

$arr_result = [];
foreach ($productData_r as $key => $val) {
    $data_stock[$key] = 0;
    $arr_detail[$key] = [];

    foreach ($stockData_r as $keys => $vals) {
        
        if($vals['productId'] == $val['productId']){
            $data_stock[$key] += $vals['stock'];

            $arr_detail[$key][$keys] = [
                'locationName'  => locationHelp($vals['locationId']),
                'stock'         => $vals['stock']
            ];
        }
    }

    $data = [
        //'productId' => $val['productId'],
        'productName'   => $val['productName'],
        'stock'         => [
            'total'  => $data_stock[$key],
            'detail' => $arr_detail[$key]
        ]
    ];
    $arr_result[$key] = $data;
}

print_r(json_encode($arr_result));

?>